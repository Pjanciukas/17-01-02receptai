<?php
session_start();

if (isset($_SESSION['user_id'])) {

    	include 'config.php';
        include 'head.php';
        include 'categories.php';

    ?>

    <?php
   $category = $_GET['category'];

    if($category == "All recipes"){

        header ("Location: user.php");
    }

    $sql = "SELECT * FROM recipes WHERE category_id= '{$category}'";
    $sql_c = "SELECT title FROM categories WHERE id = '{$category}'";

    $result = $db->query($sql);
    $result_c = $db->query($sql_c);

	if ($result->num_rows > 0) {
			$i = 0;
			while ($row = $result->fetch_assoc()){
				$data[$i] = $row;
				$i++;
		}

		}else {
			
			$data['no_entries'] = "No recipes in that category.";
		}

		?>

    <body>

    <?php include 'navigation.php';?>

	<div class="container">
		<p>Welcome! Logged in as: <?=$_SESSION['username']?> </p>
		<form action="logout.php">
        <input type="submit" name="logout" class="btn-right" value="Log out">
        </form>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> <?php while ($row = $result_c->fetch_assoc()){ echo '<h1 class="page-header"> '.$row['title'].' <br> <small class="">Filtered</small></h>';
        } ?>
                </h1>
            </div>
        </div>

        <div>
            <form method="get" action="filter.php">
                <select class="btn-right" name="category">
                    
                        <option style="color: #688843">All recipes</option>
                        <?php getCategories($db)?>
                        <input class="btn-right" type="submit" value="Filter"></input>
                    
                </select>
            </form>
        </div>
        <br><br>
                <?php if(isset($data['no_entries'])) {
    
                echo "<p>" . $data['no_entries'] . "</p>";
                }else{
                        $i = 1;
                        foreach($data as $recipe) {?>
                            <?php echo $i % 4 == 0 ? "<div class='row'>":'';?>
                            <div class="col-md-3 portfolio-item">
                                <a href="recipe.php?recipeId=<?=$recipe['id']?>&categoryId=<?=$recipe['category_id']?>">
                                    <img class="img-responsive" src="img/<?=$recipe['id'];?>.jpg" alt="">
                                </a>
                                
                                <?php   $sql = "SELECT * FROM comments WHERE recipe_id='{$recipe['id']}'";
                                        $result = $db->query($sql);
                                        $comments_count = mysqli_num_rows($result); ?>
                                <a href="recipe.php?recipeId=<?=$recipe['id']?>&categoryId=<?=$recipe['category_id']?>" class="h4"><?=$recipe['title'];?>
                                 (<span style="color: #707521"><?php printf($comments_count);?></span>)</a>
                                <p class="comments-count"></p>
                            </div>
                            <?php echo $i % 4 == 0 ? "</div>":'';?> 
                            <?php $i++;?>      
                            <?php 
                                }
                            }
                            ?>

        <?php include 'footer.php';?>

    </div>

	
<?php
} else {
		
	header("Location: index.php");
}
?>

</body>
</html>
