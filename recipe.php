<?php
session_start();

if (isset($_SESSION['user_id'])) {
	
	include 'config.php';
	include 'select-recipe-comments-category.php';

}else {
	
	header('Location: user.php');
}
?>	

<?php include 'head.php';?>

<body>

<?php include 'navigation.php';?>


    <!-- Page Content -->
    <div class="container">
    	<p>Logged in as: <?=$_SESSION['username']?> </p>
		<form action="logout.php">
        <input type="submit" name="logout" class="btn-right" value="Logout">
        </form>
            	<?php if(isset($data_recipes['no_entries'])) {
	
							echo "<p>" . $data_recipes['no_entries'] . "</p>";
							
						}else{
								foreach($data_recipes as $recipe) {
									foreach($data_category as $category){
								?>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"><?=$recipe['title']?>
                    <small><?=$category['title']?></small>
                    
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 portfolio-item width66">
                <a href="#">
                    <img class="img-responsive" src="img/<?=$recipeId;?>.jpg" alt="">
                </a>
								<div>
									<div>
										<section class="recipe-box">
										<?= nl2br($recipe['recipe_text']);?>
										</section>
									</div>
									<p class="comment-box-date">
										<?=$recipe['timestamp']; ?>
									</p>
								</div>
						<?php			
								}
							}
						}			
						?>
    			<form action="comment-db.php" method="post">
	    			<textarea name="comment-input" novalidate></textarea>
					<br>
					<input type="submit" name="comment" value="Comment" class="btn-right float-left">
					<input type="hidden" name="recipeId" value="<?=$recipe['id']?>">
					<input type="hidden" name="username" value="<?=$username['username']?>">
				</form>
        	</div>
    	</div>
    	<div>
	        	<h4>Comments:</h4>

            		<?php if(isset($data_comments['no_entries'])) {
	
							echo "<p>" . $data_comments['no_entries'] . "</p>";
							
							}else{
	
								foreach($data_comments as $comment) {
								?>
								<div>
									<div class="comment-box-date">
										<?=$comment['username'];?>
									</div>
									<div class="comment-box-date">
										<?=$comment['timestamp'];?>
									</div>
									<div class="comment-box"> 
										<?= nl2br($comment['comment']); ?>										
									</div>
								</div>
						<?php			
								}
						}			
						?>
    	</div>
    	<?php include 'footer.php';?>
	</div>
</body>
</html>
