<?php
session_start();

?>	

<?php 

include 'config.php';
include 'head.php';
include 'categories.php';

?>
        

<body>

<?php include 'navigation.php';?>

<div class="container">

	<form action="add-recipe-db.php" method="post">
		<select class="btn-right" name="category">
			<option value="" disabled selected>Select your category</option>
		    <?php getCategories($db)?>
	    </select><br><br>
	    	<input style="box-sizing: border-box; padding: 8px 20px; background-color: #fff; border: 1px solid #688843; border-radius: 4px; color: #688843;" type="text" name="title" placeholder="Add Recipe Title"><br><br>
	    <textarea name="addrecipe-input" placeholder="Add you recipe..." novalidate></textarea><br>
	    <button id="add-recipe" type="submit" class="btn-right display-inline">Add Recipe &#65291;</button>
	</form>


	<?php include 'footer.php';?>
</div>

</body>
</html>
