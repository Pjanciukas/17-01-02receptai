<?php include 'head.php';?>

<link href="css/stylecolors.css" rel="stylesheet">
<link href="css/stylelogin.css" rel="stylesheet">

<body>

<?php include 'navigation.php';?>

    <div class="container">
		<div class="square">
            <form action="login.php" method="post" class="margin-right">
                <input type="text" name="username" placeholder = "Username" class="btn-right" required>
    			<input type="password" name="password" placeholder="Password" class="btn-right" required>
    			<input type="submit" name="login" value="Login" class="btn-right"> 
    			<button id="sign-up" class="btn-right display-inline">Sign-up</button>
    		</form>
		</div>
        
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Top Recipes
                    <small>This Week's specials</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Projects Row -->
		
		<div class="square1">
		
			<div class="row">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="3"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
					<div class="item active">
					  <img src="https://upload.wikimedia.org/wikipedia/commons/6/6d/Good_Food_Display_-_NCI_Visuals_Online.jpg" style="width:100%">
					</div>

					<div class="item">
					  <img src="https://static.pexels.com/photos/5938/food-salad-healthy-lunch.jpg"  style="width:100%">
					</div>

					<div class="item">
					  <img src="https://static.pexels.com/photos/139374/pexels-photo-139374.jpeg"  style="width:100%">
					</div>

					<div class="item">
					  <img src="http://s1.1zoom.me/big0/735/Hamburger_Fast_food_485603.jpg"  style="width:100%">
					</div>
				  </div>

				  <!-- Left and right controls -->
				  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				  </a>
				</div>
			</div>	
		</div>
		<br>
		<br>
		<br>
		<br>
		<div class="row">
            <div class="col-md-3 portfolio-item">
                <a href="recipe.php">
                    <img class="img-responsive" src="https://img-global.cpcdn.com/001_recipes/2aa5df699ad24a58/664x470cq70/photo.jpg" alt="">
                </a>
                <h5>Healthy Food</h5>
                <h4>Salmon and Pesto-Dressed Vegetables</h4>
            </div>
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://3.bp.blogspot.com/-4QJy8GyVDfE/Um68BPlb90I/AAAAAAAACgY/H0xTc_pzcXs/s1600/p1.png" alt="">
                </a>
                <h5>Pizza and Pasta</h5>
                <h4>Squash and Spinach Rotolo</h4>
            </div>
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://www.seriouseats.com/recipes/assets_c/2014/09/20140918-jamie-olivers-comfort-food-insanity-burger-david-loftus-thumb-1500xauto-411285.jpg" alt="">
                </a>
                <h5>Burgers and Sandwiches</h5>
                <h4>The Insanity Burger</h4>
            </div>
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://cdn.jamieoliver.com/news-and-features/features/wp-content/uploads/sites/2/2015/11/xmasturkeytips.jpg" alt="">
                </a>
                <h5>Special Occasions</h5>
                <h4>Perfect Roasted Turkey</h4>
            </div>
        </div>
        <!-- /.row -->

        <!-- Projects Row -->
        <div class="row">
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="https://annasappetite.files.wordpress.com/2011/05/img_0929.jpg" alt="">
                </a>
                <h5>Pizza and Pasta</h5>
                <h4>Home-Made Pizza with 3 Toppings</h4>
            </div>
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="https://cdn.wyza.com.au/media/1586/bay-salt-prawn-skewers-with-summer-veg-retirementlivingtv.gif" alt="">
                </a>
                <h5>Seafood</h5>
                <h4>Bay Salt Prawn Skewers with Summer Vegetables</h4>
            </div>
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://17374-presscdn-0-15.pagely.netdna-cdn.com/wp-content/uploads/2014/10/jamie-oliver-steak-sandwiches-recipe.jpg" alt="">
                </a>
                <h5>Burgers and Sandwiches</h5>
                <h4>Next-Level Steak and Onion Sandwich</h4>
            </div>
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://goodtoknow.media.ipcdigital.co.uk/111/00000b3ac/5635/Jamie-Oliver-curry-recipe.jpg" alt="">
                </a>
                <h5>Soups</h5>
                <h4>Magnificient Prawn Curry</h4>
            </div>
        </div>
        <!-- /.row -->

        <!-- Projects Row -->
        <div class="row">
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://img.delicious.com.au/SHqOf3Sh/h480-w720-cfill/del/2016/06/jamie-olivers-chicken-ham-and-leek-pie-31273-2.jpg" alt="">
                </a>
                <h5>Meat Pies</h5>
                <h4>Chicken, Ham and Leek Pie</h4>
                            </div>
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://cdn.jamieoliver.com/news-and-features/features/wp-content/uploads/sites/2/2014/08/feature-header22.jpg" alt="">
                </a>
                <h5>Desserts</h5>
                <h4>Epic Vegan Chocolate Cake</h4>
            </div>
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://img.delicious.com.au/Afg0kEFu/h506-w759-cfill/del/2016/02/blackcurrant-meringue-pies-27106-1.jpg" alt="">
                </a>
                <h5>Desserts</h5>
                <h4>Blackcurrant Meringue Pies</h4>
            </div>
            <div class="col-md-3 portfolio-item">
                <a href="#">
                    <img class="img-responsive" src="http://cdn.jamieoliver.com/news-and-features/features/wp-content/uploads/sites/2/2014/12/christmas-drinks-header.jpg" alt="">
                </a>
                <h5>Cocktails</h5>
                <h4>Hot Toddy</h4>
            </div>
        </div>
    <?php include 'footer.php';?>

    </div>
</body>
</html>
