<?php
session_start();

if (isset($_SESSION['user_id'])) {

    	include 'config.php';
        include 'select-all-recipies.php';
        include 'head.php';
        include 'categories.php';
    ?>

    <body>

    <?php include 'navigation.php';?>

	<div class="container">
		<p>Welcome! Logged in as: <?=$_SESSION['username']?> </p>
		<form action="logout.php">
        <input type="submit" name="logout" class="btn-right" value="Log out">
        </form>

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Top Recipes
                    <small>Vilnius Coding School specials</small>
                </h1>
            </div>
        </div>

        <div>
            <form method="get" action="filter.php">
                <select class="btn-right" name="category">
                        <option value="" disabled selected>Select your category</option>
                        <option style="color: #688843">All recipes</option>
                        <?php getCategories($db)?>
                        <input class="btn-right" type="submit" value="Filter"></input>
                </select>
            </form>
        </div>
        <br><br>

        <?php if(isset($data['no_entries'])) {
    
                echo "<p>" . $data['no_entries'] . "</p>";
                }else{
                        $i = 1;
                        foreach($data as $recipe) {?>
                            <?php echo $i % 4 == 0 ? "<div class='row'>":'';?>
                            <div class="col-md-3 portfolio-item">
                                <a href="recipe.php?recipeId=<?=$recipe['id']?>&categoryId=<?=$recipe['category_id']?>">
                                    <img class="img-responsive" src="img/<?=$recipe['id'];?>.jpg" alt="">
                                </a>
                                <h5><?=$recipe["category_title"]?></h5>
                                <?php   $sql = "SELECT * FROM comments WHERE recipe_id='{$recipe['id']}'";
                                        $result = $db->query($sql);
                                        $comments_count = mysqli_num_rows($result); ?>
                                <a href="recipe.php?recipeId=<?=$recipe['id']?>&categoryId=<?=$recipe['category_id']?>" class="h4"><?=$recipe['title'];?>
                                 (<span style="color: #707521"><?php printf($comments_count);?></span>)</a>
                                <p class="comments-count"></p>
                            </div>
                            <?php echo $i % 4 == 0 ? "</div>":'';?> 
                            <?php $i++;?>      
                            <?php 
                                }
                            }
                            ?>
            <form action="add-recipe.php" method="post" >
            <button id="add-recipe" type="submit" class="btn-right display-inline">Add Recipe &#65291;</button>
            </form>

        <?php include 'footer.php';?>
    </div>

	
<?php
} else {
		
	header("Location: index.php");
}
?>

</body>
</html>


